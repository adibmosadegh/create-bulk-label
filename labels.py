import gitlab


def main():
    labels = getTextFilePath()
    token = "xxxxxxxxxxxxxx"
    host = "(gitlab.com)"
    login(host, token, labels)


def get_text_file_path():
    print("please enter the path of your text file: ")
    file_name = input()
    labels = []
    file = open(file_name, 'r')
    lines = file.readlines()
    for line in lines:
        labels.append(line)
    return labels


def login(host, token, labels):
    git_login = gitlab.Gitlab(host, private_token=token)
    project = git_login.projects.get('projectName')
    for labelName in labels:
        project.labels.create({'name': labelName, 'color': '#8899aa'})


main()
